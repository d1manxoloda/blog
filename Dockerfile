FROM python:3.9-alpine
ARG MTS_VN=456
ENV MTS=789

WORKDIR  /code
COPY . /code

RUN pip install --verbose -r requirements.txt

ENTRYPOINT [ "python3" ]
CMD ["app.py"]

EXPOSE 5000
